<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Seller;
use App\Http\Controllers\SellerPurchase;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('sellers', [Seller::class, 'index']);

Route::post('seller', [Seller::class, 'store']);

Route::post('purchase', [SellerPurchase::class, 'store']);

Route::get('purchase', [SellerPurchase::class, 'index']);


