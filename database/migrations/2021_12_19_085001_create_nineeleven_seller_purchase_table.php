<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNineelevenSellerPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nineeleven_seller_purchase', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sellerid')->unique();
            $table->string('billnumber');
            $table->double('amount', 20, 2);
            $table->date('billdate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nineeleven_seller_purchase');
    }
}
