<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SellerPurchaseModel;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerPurchase extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(SellerPurchaseModel::with('sellerDetails')->get(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $purchase = SellerPurchaseModel::where('sellerid', $request->sellerid)->where('billnumber', $request->billnumber)->get();
            if (count($purchase)> 0) {
                return response()->json("Duplicate entry", 400);
            } else {
                $sellerPurchase = SellerPurchaseModel::create($request->all());
                if ($sellerPurchase) {
                    return response()->json('Saved Successfully', 201);
                } else {
                    return response()->json("Try Again", 500);
                }
            }
        } catch (\Exception $e){
            // throw new HttpException(500, $e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
