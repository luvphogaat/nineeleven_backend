<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerModels extends Model
{
    use HasFactory;
    protected $table = 'nineeleven_seller';

    protected $fillable = [
        'seller_name',
        'seller_address',
    ];
}
