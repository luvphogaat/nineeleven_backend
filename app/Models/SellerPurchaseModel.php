<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerPurchaseModel extends Model
{
    use HasFactory;
    protected $table = 'nineeleven_seller_purchase';

    protected $fillable = [
        'sellerid',
        'billnumber',
        'amount',
        'billdate'
    ];

    public function sellerDetails() {
        return $this->belongsTo('App\Models\SellerModels', 'sellerid', 'id');
    }
}
